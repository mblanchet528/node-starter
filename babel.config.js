const isTest = process.env["NODE_ENV"] === "test";
const ignore = isTest ? [] : ["src/**/*.test.js"];

module.exports = api => {
  api.cache(true);

  return {
    presets: ["@babel/preset-env"],
    plugins: [
      [
        "module-resolver",
        {
          root: ["./src/"],
          alias: { "@": "./src" },
        },
      ],
    ],
    ignore,
  };
};
